import React from 'react';
import RadioPlayer from './components/RadioPlayer/RadioPlayer';
import './App.scss';

const App: React.FC = () => {
  return (
    <div className="App">
      <RadioPlayer />
    </div>
  );
}

export default App;
