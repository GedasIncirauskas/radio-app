import * as React from 'react';
import Controller from './../Controller/Controller';
import BackButton from '../../assets/back-arrow.png';
import Switch from '../../assets/switch.png';
import './RadioPlayer.scss';

interface IState {
	data: Array<data>,
	radioName: string
}

interface data {
	name: string,
	number: string,
	isOpened: boolean
}

class RadioPlayer extends React.Component<{}, IState> {
	constructor(props: {}) {
		super(props);
		this.changeRadioPlus = this.changeRadioPlus.bind(this);
		this.changeRadioMinus = this.changeRadioMinus.bind(this);
	}

	state = {
		data: [
			{name: 'Putin FM', number: '66,6', isOpened: false},
			{name: 'Dribbble FM', number: '101,2', isOpened: false},
			{name: 'Doge FM', number: '94,4', isOpened: false},
			{name: 'Ballads FM', number: '87,1', isOpened: false},
			{name: 'Maximum FM', number: '142,2', isOpened: false}
		],
		radioName: ''
	};

	fakeClick = () => {
		console.log('CLICKED!!!')
	};

	toogleHandler (index: number) {
		let newArray = [...this.state.data];
		newArray[index].isOpened = !newArray[index].isOpened;
		this.setState({
			data: this.state.data,
			radioName: newArray[index].name
		})
	};

	changeRadioPlus (currentIndex: number) {
		let newData = this.state.data.map((item: {name: string, number: string, isOpened: boolean}, index: number) => {
			item.isOpened = false
			if((currentIndex + 1) === index) {
				item.isOpened = true
			}
			return item
		})
		this.setState({
			data: newData,
			radioName: this.state.data[currentIndex +1].name
		})
	};

	changeRadioMinus (currentIndex: number) {
		let newData = this.state.data.map((item: {name: string, number: string, isOpened: boolean}, index: number) => {
			item.isOpened = false
			if((currentIndex -1) === index) {
				item.isOpened = true
			}
			return item
		})
		this.setState({
			data: newData,
			radioName: this.state.data[currentIndex -1].name
		})
	};

	render () {
		return (
			<div className="Container">
				<div className="Header">
					<span className="First-image">
						<img onClick={this.fakeClick} src={BackButton} alt="back-button" /></span>
					<span><h3>STATIONS</h3></span>
					<span className="Second-image">
						<img onClick={this.fakeClick} src={Switch} alt="switch-button" /></span>
					<div className="Body-style">
					{this.state.data.map((item:{name: string, number: string, isOpened: boolean}, index:number) => (
						<div key={index}>
							<div id="myStyle" onClick={() => this.toogleHandler(index)}>{item.name}</div>
						    <div id="myStyle2" onClick={() => this.toogleHandler(index)}><strong>{item.number}</strong></div>
							{item.isOpened  ?
								<Controller
									totalRadio={this.state.data.length}
									index={index}
									minus={this.changeRadioMinus}
									plus={this.changeRadioPlus}
								/> : null
							}
						</div>
						))}
						<div className="Footer-style">
							<strong>CURRENTLY PLAYING</strong>
							<p>{this.state.radioName}</p>
						</div>
					</div>
				</div>
			</div>
		)
	};
}


export default RadioPlayer;