import React, { PropsWithChildren } from 'react';
import Plus from '../../assets/plus.png';
import Minus from '../../assets/minus.png';
import Logo from '../../assets/logo.jpeg';
import './Controller.scss';

interface IProps {
    minus:  Function,
    plus: Function,
    index: number,
    totalRadio: number
}

const controller: React.FC<IProps> = (props:PropsWithChildren<IProps>) => {
    return (
            <div className="Control-style">
                {props.index <= props.totalRadio - props.totalRadio ?
                    <img id="First-img-disable" src={Minus} alt="Minus-button" /> :
                    <img id="First-img"  src={Minus} alt="Minus-button" onClick={() => props.minus(props.index)}/>
                }
                <img id="Middle-img" src={Logo} alt="Logo" />
                {props.index >= props.totalRadio - 1 ?
                    <img id="Last-img-disable" src={Plus} alt="Plus-button" /> :
                    <img id="Last-img" src={Plus} alt="Plus-button" onClick={() => props.plus(props.index)}/>
                }
        </div>
    );
};

export default controller;